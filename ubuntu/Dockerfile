FROM ubuntu:20.04
LABEL maintainer="steven@carboncollins.uk" \
      stack=proxima \
      distro=ubuntu

ARG timezone="Europe/Stockholm"

ENV DEBIAN_FRONTEND=noninteractive

# Set locale
RUN apt-get update && apt-get -y install locales && apt-get clean
RUN sed -i '/en_GB.UTF-8/s/^# //g' /etc/locale.gen && sed -i '/sv_SE.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LANG=en_GB.UTF-8 \
    LANGUAGE=en_GB.UTF-8 \
    LC_CTYPE=en_GB.UTF-8 \
    LC_ALL=en_GB.UTF-8

# Set local time zone
RUN apt-get update && apt-get install -y tzdata && apt-get clean
RUN ln -fs /usr/share/zoneinfo/${timezone} /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

# Common proxima dependencies
RUN apt-get update && apt-get install -y ca-certificates curl tar gnupg2 software-properties-common lsb-release && apt-get clean

# Clean Caches

CMD ["echo", "Proxima ubuntu base image"]
